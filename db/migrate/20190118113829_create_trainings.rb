class CreateTrainings < ActiveRecord::Migration[5.2]
  def change
    create_table :trainings do |t|
      t.datetime :date
      t.string :name

      t.timestamps
    end
  end
end
